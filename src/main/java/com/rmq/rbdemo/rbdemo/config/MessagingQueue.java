package com.rmq.rbdemo.rbdemo.config;

import java.io.IOException;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.Connection;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.rabbitmq.client.Channel;

@Configuration
public class MessagingQueue {

    /**
     *
     */
    public static final String ITEM_QUEUE = "item-queue";
    /**
     *
     */
    public static final String CUSTOMER_ROUTING_KEY = "customer-routing-key";

    public static final String ITEM_ROUTING_KEY = "item-routing-key";
    /**
     *
     */
    public static final String CUSTOMER_QUEUE = "customer-queue";
    /**
     *
     */
    public static final String EXCHANGE = "dj-data-exchange";

    @Bean(CUSTOMER_QUEUE)
    public Queue customerQueue() {
        return new Queue(CUSTOMER_QUEUE);
    }

    @Bean(ITEM_QUEUE)
    public Queue itemQueue() {
        return new Queue(ITEM_QUEUE);
    }

    @Bean
    public TopicExchange exchange() {
        return new TopicExchange(EXCHANGE);
    }

    // @Bean
    // public Binding binding(Queue queue, TopicExchange exchange) {
    // return BindingBuilder.bind(queue).to(exchange).with(CUSTOMER_ROUTING_KEY);
    // }

    @Bean
    public Channel channel(ConnectionFactory connectionFactory) {
        Connection con = connectionFactory.createConnection();

        Channel channel = con.createChannel(false);
        // try {
        // channel.queueDeclarePassive(CUSTOMER_QUEUE);
        // channel.queueDeclarePassive(ITEM_QUEUE);
        // } catch (IOException e1) {
        // // TODO Auto-generated catch block
        // e1.printStackTrace();
        // }

        try {
            channel.queueBind(CUSTOMER_QUEUE, EXCHANGE, CUSTOMER_ROUTING_KEY);
            channel.queueBind(ITEM_QUEUE, EXCHANGE, ITEM_ROUTING_KEY);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return channel;
    }

    @Bean
    public MessageConverter converter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        final RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(converter());
        return template;
    }

}
