package com.rmq.rbdemo.rbdemo.publisher;

import static com.rmq.rbdemo.rbdemo.config.MessagingQueue.CUSTOMER_ROUTING_KEY;
import static com.rmq.rbdemo.rbdemo.config.MessagingQueue.EXCHANGE;
import static com.rmq.rbdemo.rbdemo.config.MessagingQueue.ITEM_ROUTING_KEY;

import java.util.UUID;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rmq.rbdemo.rbdemo.dto.Customer;
import com.rmq.rbdemo.rbdemo.dto.Item;
import com.rmq.rbdemo.rbdemo.dto.ObjectStatus;

@RestController
@RequestMapping("/data")
public class DataPublisher {

    private RabbitTemplate template;

    DataPublisher(RabbitTemplate template) {
        this.template = template;
    }

    @PostMapping("/customer")
    public String addCustomer(@RequestBody Customer customer) {
        customer.setId(UUID.randomUUID().toString());
        ObjectStatus status = new ObjectStatus(customer, "Add in progress", "Customer added ");
        template.convertAndSend(EXCHANGE, CUSTOMER_ROUTING_KEY, status);
        return "Customer added to Queue";
    }

    @PostMapping("/item")
    public String addCustomer(@RequestBody Item item) {
        item.setId(UUID.randomUUID().toString());
        ObjectStatus status = new ObjectStatus(item, "Add in progress", "Item added ");
        template.convertAndSend(EXCHANGE, ITEM_ROUTING_KEY, status);
        return "Item added to Queue";
    }

}
