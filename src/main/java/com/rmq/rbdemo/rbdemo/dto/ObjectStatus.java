package com.rmq.rbdemo.rbdemo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ObjectStatus {

    private Object customer;

    private String status;

    private String message;

}
