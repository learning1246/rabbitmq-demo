package com.rmq.rbdemo.rbdemo.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.rmq.rbdemo.rbdemo.config.MessagingQueue;
import com.rmq.rbdemo.rbdemo.dto.ObjectStatus;

@Component
public class ConsumerService {

    @RabbitListener(queues = MessagingQueue.CUSTOMER_QUEUE)
    public void consumeCusotmerMessageFromQueue(ObjectStatus status) {
        System.out.println("Customer received from queue" + status);
    }

    @RabbitListener(queues = MessagingQueue.ITEM_QUEUE)
    public void consumeMessageFromQueue(ObjectStatus status) {
        System.out.println("Item received  from queue" + status);
    }

}
