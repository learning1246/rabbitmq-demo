package com.rmq.rbdemo.rbdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RbdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(RbdemoApplication.class, args);
	}

}
